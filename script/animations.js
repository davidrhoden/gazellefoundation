// On and off animations
function panelOn (panel) {
	
switch (panel) {

case 1:
	howCanAShovel.animate({x: 0, y:30, width: 221, height: 100}, 200, "linear", 
		function () {
			ourStoryBegins = paper1.image("png/Our-Story-Begins.png", 280, 80, 194, 71)
				.attr({opacity: 0});
			ourStoryBegins.animate({opacity: 1}, 500, "linear", function () {
				burundi = paper1.image("png/Burundi.png", 700, -100, 222, 56);
				burundi.animate({x: 280, y: 160}, 1000, "backOut", function () {
					africa = paper1.image("png/africa.png", 500, 70, 362, 304);
					burundiMap = paper1.image("png/Burundi-Map.png", 696, 258, 6.96, 8.06);
					burundiMap.animate({width: 115, height: 133}, 500);	
			});
		});	
	});
break;

case 2:
	beautifulLand = paper2.image("png/The-Land-Is-Beautiful.png", 0, 30, 190, 68).attr({opacity: 0});
	photoCircle1 = paper2.image("png/photoCircle1.png", 0, 150, 282, 271).attr({opacity: 0});
	photoCircle2 = paper2.image("png/photoCircle2.png", 50, 190, 181, 181).toBack().attr({opacity: 0});
	photoCircle3 = paper2.image("png/photoCircle3.png", 45, 190, 197, 187).toBack().attr({opacity: 0});
	photoCircle4 = paper2.image("png/photoCircle4.png", 45, 190, 164, 164).toBack().attr({opacity: 0});
	photoLine1 = paper2.path("M141,285").attr({stroke: "white", "stroke-width": 6}).toBack();
	photoLine2 = paper2.path("M141,285").attr({stroke: "white", "stroke-width": 6}).toBack().attr({opacity: 0});
	photoLine3 = paper2.path("M141,285").attr({stroke: "white", "stroke-width": 6}).toBack().attr({opacity: 0});
	beautifulLand.animate({opacity: 1}, 200, "linear");
	photoCircle1.animate({opacity: 1}, 1000, "linear", function () {
		photoCircle2.attr({opacity: 1}).animate({x: 260, y:5}, 1000, "linear");
		photoLine1.animateWith(photoCircle2, null, {path: "M141,285L350,95"}, 1000, "linear", function () {
			photoLine2.attr({opacity: 1}).animateWith(photoCircle3, null, {path: "M141,285L498,285"}, 1000, "linear");
			photoCircle3.attr({opacity: 1}).animate({x: 400, y:180}, 1000, "linear", function () {
				photoCircle4.attr({opacity: 1}).animate({x: 270, y:320}, 1000, "linear");
				photoLine3.attr({opacity: 1}).animateWith(photoCircle4, null, {path: "M141,285L352,402"}, 1000, "linear");
			});
		});
	});
break;

case 3:
	butTheNation = paper3.image("png/But-the-Nation.png", 20, -100, 286, 67);
	world = paper3.image("png/World.png", 19, 600, 288, 80);
	butTheNation.animate({y: 30}, 1000);
	world.animateWith(butTheNation, null, {y:105}, 1000, function () {
		ranked = paper3.image("png/ranked.png", 24, 195, 290, 29);
		ruler = paper3.image("png/poverty-ruler.png", 25, 230, 283, 27);
		blue = paper3.image("png/poverty-ruler-blue.png", 25, 233, 1, 22).attr({opacity: .9});
		blue.animate({width: 278}, 1850);
		$('#odometer').jOdometer({increment: 1, counterStart:'001', counterEnd:'185', 
			numbersImage: 'png/odometer-numbers.png', spaceNumbers: 0, offsetRight:5, heightNumber: 32, 
			widthNumber:18, delayTime: 10, speed: 1 });
	});	
break;

case 4:
	burundiansLack = paper4.image("png/Burundians-Lack.png", 10, 50, 321, 133).attr({opacity: 0});
	arrow = paper4. image("png/white-arrow.png", 278, 131, 55, 47).attr({opacity: 0});
	arrow.animateWith(burundiansLack, null, {opacity: 1}, "linear");
	burundiansLack.animate({opacity: 1}, 1000, "linear", function () {
		arrow.animate({x: 288}, 800, "bounce", function () {
			water = paper4.image("png/Just-Water.png",427, 350, 138, 60);
			germs = paper4.image("png/Germs.png",430, 250, 125, 126);
			germs.attr({opacity: 0});
			glass = paper4.image("png/Water-Glass-Blue-Base.png",400, 150, 192, 264);
			glassShadow = paper4.image("png/Water-Glass-Shadow.png",397, 150, 198, 272).toBack();
			water.animate({x: 413, y: 221, width: 167, height: 194}, 1500, "linear", function () {
				germs.animate({opacity: 1}, 1000, "linear", function () {
					illnesses = paper4.image("png/illnesses.png", 792, 60, 119, 25).attr({opacity: 0});
					hospitalizations = paper4.image("png/hospitalizations.png", 1080, 187, 197, 25).attr({opacity: 0});
					mortality = paper4.image("png/infant-mortality.png", 830, 360, 192, 25).attr({opacity: 0});
					tombstones = paper4.image("png/tombstones.png", 1050, 395, 183, 113).attr({opacity: 0});
					magGerm = paper4.image("png/Germs-Magnified.png",520, 325, 20, 20);
					magGerm2 = paper4.image("png/Germs-Magnified.png",520, 325, 20, 20);
					line1=paper4.path("M525,327").attr({stroke: "#ff6f28"});
					line2=paper4.path("M530,344").attr({stroke: "#ff6f28"});
					magGerm.animate({x: 620, y: 200, width: 131, height: 131}, 1500, "bounce", function () {
						circle1=paper4.image("png/Green-Circle.png", 615, 195, 141, 141).toBack();
						slime1=paper4.image("png/Slime-Drip-1.png", 635, 250, 94, 62).toBack();
						tendril1=paper4.image("png/Slime-Tendril-1.png", 732, 210,138, 108).toBack().attr({opacity: 0});
						tendril2=paper4.image("png/Slime-Tendril-2.png", 828, 102 ,223, 220).toBack().attr({opacity: 0});
						circle2=paper4.image("png/green-circle-80.png", 954, 118, 1, 1);
						circle3=paper4.image("png/green-circle-50.png", 1015, 197, 1, 1);
						circle4=paper4.image("png/green-circle-20.png", 928, 194, 1, 1);
						germShadow = paper4.image("png/Germs-Shadow.png",615, 70, 470, 350).attr({opacity: 0}).toBack();
						circle2.animate({x: 774, y: 88, width: 159, height: 159}, 800, "backOut", function () {
							circle3.animate({x: 956, y: 138, width: 119, height: 119}, 800, "backOut", function () {
								circle4.animate({x: 872, y: 238, width: 113, height: 113}, 800, "backOut", function () {
									tombstones.animate({opacity: 1}, 800);	
								});
								mortality.animateWith(circle4, null, {opacity: 1}, 800);
								slime1.animate({y: 312}, 1500, ">");
								tendril1.animateWith(slime1, null, {opacity: 1}, 2000);
								tendril2.animateWith(slime1, null, {opacity: 1}, 2000);
								germShadow.animateWith(slime1, null, {opacity: 1}, 2000);
							});
							hospitalizations.animateWith(circle3, null, {opacity: 1}, 800);
						});
						illnesses.animateWith(circle2, null, {opacity: 1}, 800);
						
					});
					line1.animateWith(magGerm, null, {path: "M525,327L639,220"}, 1500, "bounce");
					line2.animateWith(magGerm, null, {path: "M530,344L690,330"}, 1500, "bounce");
				});
			});
		});
	});
break;

case 5:
	accessing = paper5.image("png/accessing-clean-water.png", 0, 100, 205, 165).attr({opacity: 0});
	man = paper5.image("png/man-carrying-water.png", 0, 441, 79, 83).attr({opacity: 0});
	accessing.animate({opacity: 1}, 1000, function() {
		man.animate({opacity: 1}, 1000);
	});
	curveArrow = paper5.image("png/footsteps-arrow.png", 210, 100, 108, 130);
	steps = paper5.image("png/footsteps-1.png", 211, 89, 104, 133);
	miles = paper5.image("png/miles-a-day.png", 570, 700, 183, 163);
	setTimeout(function () { steps.node.href.baseVal = "png/footsteps-1.png";
		setTimeout(function () { steps.node.href.baseVal = "png/footsteps-2.png";
			setTimeout(function () { steps.node.href.baseVal = "png/footsteps-3.png";
				 setTimeout(function () { steps.node.href.baseVal = "png/footsteps-4.png";
					setTimeout(function () { steps.node.href.baseVal = "png/footsteps-5.png";
						setTimeout(function () { steps.node.href.baseVal = "png/footsteps-6.png";
							setTimeout(function () { steps.node.href.baseVal = "png/footsteps-7.png";
								walkingUp = paper5.image("png/walking-up-to.png", 330, 80, 418, 64);
								$("#flip15").css("display", "block");
								$('#flip15').sprite({fps: 12, no_of_frames: 6, play_frames: 6 });
								miles.animate({y: 167}, 1000, "backOut");
							}, 500);
						}, 500);
					}, 500);
				 }, 500);
			}, 500);
		}, 500);
	}, 500);	

break;

case 6:
	circleOr = paper6.image("png/blue-or.png", 0, -100, 74, 74);
	circleOr.animate({y: 215}, 500, "linear", function () {
		number264 = paper6.image("png/264.png", 168, 133, 156, 107);
		number264.animate({x: 114, y: 80, width: 312, height: 214}, 2000, "bounce");
		field = paper6.image("png/football-field-no-goalposts.png", 430, 142, 391, 1);
		field.animate({height: 170}, 800, "backOut", function () {
			footballText = paper6.image("png/football-fields-text.png", 114, 700, 701, 84);
			footballText.animate({y: 320}, 500, "linear", function() {
				smPost = paper6.image("png/small-goalpost.png", 594, -100, 59, 71);
				lgPost = paper6.image("png/large-goalpost.png", 556, -180, 137, 162).toFront();
				smPost.animate({y: 73}, 500, "linear", function () {
					lgPost.animate({y: 125}, 500, "linear");
				});
			});
		});																  
	});
break;

case 7:
	carryText = paper7.image("png/carrying-water.png", 30, 880, 317, 125);
	jug = paper7.image("png/40-lbs-jug.png",42, 956, 50, 71);
	carryText.animate({y: 80}, 1000, "backOut");
	jug.animateWith(carryText, null, {y: 156}, 1000, "backOut", function() {
		fulcrum = paper7.image("png/fulcrum.png",136, 492, 70, 45);
		lever = paper7.image("png/balance-right.png", 30, 455, 277, 85);
		var jugAni = Raphael.animation({y: 390}, 1000, "linear", function () {
			jug.animate({y: 423}, 300, "linear", function () {
				jugShadow = paper7.image("png/jug-shadow.png", 0, 488, 82, 17);
			});
			lever.animateWith(jug, null, {transform: "r-15"}, 500, "bounce");												
		});
		jug.animate(jugAni.delay(1000));
		forty = paper7.image("png/40.png", 49, 160, 42, 58).toBack(); 
	});
	
break;

case 8:
	skippingSchool = paper8.image("png/skipping-school.png", 0, 80,  375, 49).attr({"clip-rect": "0 80 0 49" });
	literacy = paper8.image("png/67-percent-read.png", 203, 138, 167, 91).attr({"clip-rect": "203 138 167 0" });
	orWork = paper8.image("png/or-work.png", 390, 78, 214, 50).attr({"clip-rect": "390 78 0 50" });
	avgIncome = paper8.image("png/avg-income.png", 615, 78, 291, 50).attr({"clip-rect": "615 78 0 50" });
	dollars = paper8.image("png/600-dollars-year.png", 607, 162, 297, 78).attr({"clip-rect": "607 162 297 0" });
	skippingSchool.animate({"clip-rect" : "0 80 375 49"}, 1000, function () {
		schoolKids.animate({opacity: 0}, 1000, function () {
			literacy.animate({"clip-rect" : "203 138 167 91"}, 1000, function () {
				orWork.animate({"clip-rect" : "390 78 291 50"}, 1000, function () {
					farmers.animate({opacity: 0}, 1000, function () {
						crops.animate({opacity: 0}, 1000, function () {
							pigs.animate({opacity: 0}, 1000, function () {
								avgIncome.animate({"clip-rect" : "615 78 291 50"}, 1000, function () {
									dollars.animate({"clip-rect" : "607 162 297 78"}, 1000);
								});
							});
						});
					});
				});
			}); 
		});
	});
break;

case 9:
	youBig = paper9.image("png/you-500-percent.png", 0, 81, 321, 197);
	youBig.animate({width: 71, height: 39}, 1000, "backIn", function () {
		canChange = paper9.image("png/can-change.png", 76, 80, 384, 39);
		sunShovel = paper9.image("png/shovel-no-sunburst.png", 548, 180, 100, 100);
		sunBurst = paper9.image("png/sunburst.png", 548, 180, 100, 100);
		sunBurst.animateWith(sunShovel, null, {width: 319, height: 319, x: 441, y: 80}, 1000, "linear");
		sunShovel.animate({width: 319, height: 319, x: 441, y: 80}, 000, "linear", function () {
			rotateSunburstA(360);
			shovelText = paper9.image("png/shovels-in-hands.png", 0, 128, 440, 106).attr({opacity: 0});	
			shovelText.animate({opacity: 1}, 1000);
		});
		
	});
	function rotateSunburstA (degree) {
		sunBurst.animate({transform: "r"+degree}, 4000, "linear", function () {rotateSunburstB(degree+360); });
	}
	function rotateSunburstB (degree) {
		sunBurst.animate({transform: "r"+degree}, 4000, "linear", function () {rotateSunburstA(degree+360); });
	}
break;

case 10:
	birds = paper10.image("png/birds.png", 0, 239, 208, 133).attr({"clip-rect": "0 0 500 250"});
	pipe1 = paper9.image("png/pipe1.png", 532, 525, 247, 75).attr({"clip-rect": "770 525 10 10"});
			pipe1.animate({"clip-rect": "748 525 32 60"}, 1000, function () {
				pipe1.animate({"clip-rect": "532 525 248 60"}, 1000, function () {
					pipe1.animate({"clip-rect": "532 525 248 75"}, 500, function () {
						well = paper9.image("png/empty-well.png", 718, -72, 121, 72);
						well.animate({y: 464}, 1000, ">", function() {
							water1 = paper9.image("png/water-pipe1.png", 532, 525, 247, 75)
								.attr({"clip-rect": "532 590 10 10"});
							well.toFront();
							water1.animate({"clip-rect": "532 568 41 32"}, 500, function () {
								water1.animate({"clip-rect": "532 568 248 32"}, 1000, function () {
									water1.animate({"clip-rect": "532 525 247 75"}, 1000, function () {
										wellWater = paper9.image("png/well-water.png", 731, 471, 94, 6);
										birds.animate({x: 250, y: -150}, 3000, "linear");											
										atTheWell = paper9.image("png/at-the-well.png", 814, 378, 178, 154)
											.attr({opacity: 0});
										atTheWell.animate({opacity: 1}, 500, "linear", function () {
											pipe2 = paper10.image("png/pipe2.png", 0, 532, 894, 71)
												.attr({"clip-rect": "0 532 10 10"});
											pipe2.animate({"clip-rect": "0 532 10 71"}, 500, function () {
												pipe2.animate({"clip-rect": "0 532 894 71"}, 1500);	
												childrenReturn = paper10.image("png/children-return.png", 336, 51, 635, 133)
													.attr({opacity: 0});
												womenReturn = paper10.image("png/women-return.png", 343, 168, 578, 133)
													.attr({opacity:0});
												childrenReturn.animate({opacity: 1}, 1000, "linear", function() {
													womenReturn.animate({opacity: 1}, 1000, "linear");														  
												});
											});														 
										});
									});
								});
							});
						});
					});
				});															  
			});
break;

case 11:
	growText = paper11.image("png/economy-grow.png", 0, 790, 506, 185);
	growText.animate({y: 80}, 1200, function () {
		arrow = paper11.path("M493.5,210").attr({stroke: "#FF6633", "stroke-width": 7.75,
					"arrow-end": "classic"});
		arrow.animate({path: "M493.5,210L523,10"}, 1000);
	});
break;

} // end of switch

} // end of panelOn



function panelOff(panel) {
	
switch (panel) {

case 1:
	burundiMap.animate({transform: "s1"}, 1000, function () {
		burundiMap.remove();
		africa.remove();												  
	});
	burundi.animate({y: -160}, 2000, function () {
		burundi.remove();
		ourStoryBegins.remove()
	});
	ourStoryBegins.animateWith(burundi,null, {y:-160}, 2000, function() {
		burundiMap.remove();
		africa.remove();
	});
break;

case 2:
	beautifulLand.animate({opacity: 0}, 200, "linear");
	photoLine1.remove();
	photoLine2.remove();
	photoLine3.remove();
	photoCircle1.animate({y: -500}, 1000, "backIn", function () { this.remove();});
	photoCircle2.animate({y: -500}, 1000, "backIn", function () { this.remove();});
	photoCircle3.animate({y: -500}, 1000, "backIn", function () { this.remove();});
	photoCircle4.animate({y: -500}, 1000, "backIn", function () { this.remove();});
break;

case 3:
	butTheNation.animate({y: 1000}, 1000, "linear", function () { 
		this.remove();
		ranked.remove();
		ruler.remove();
		blue.remove();
		$('#odometer').html('');
	});
	world.animateWith(butTheNation, null, {y: 1000}, 1000, "linear", function () { this.remove();});
	try {
		ruler.animateWith(butTheNation, null, {y: 1000}, 1000, "linear", function () { this.remove();});
		blue.stop().animateWith(butTheNation, null, {y: 1000}, 1000, "linear", function () { this.remove();});
		ranked.animateWith(butTheNation, null, {y: 1000}, 1000, "linear", function () { this.remove();});
		$('#odometer').html('');
	} catch(e) {}
break;

case 4:
	line1.remove();
	line2.remove();
	set4 = paper4.set();
	set4.push(burundiansLack);
	set4.push(water);
	set4.push(germs);
	set4.push(glass);
	set4.push(glassShadow);
	set4.push(magGerm);
	set4.push(magGerm2);
	set4.push(line1);
	set4.push(line2);
	set4.push(circle1);
	set4.push(slime1);
	set4.push(tendril1);
	set4.push(tendril2);
	set4.push(circle2);
	set4.push(circle3);
	set4.push(circle4);
	set4.push(germShadow );
	set4.push(illnesses);
	set4.push(hospitalizations);
	set4.push(mortality);
	set4.push(tombstones);
	set4.animate({opacity: 0}, 2000, "linear", function () {this.remove();} );
	
break;

} // end of switch
} // end of panelOff function


/*Function for creating a circle with clock rotation
paperGlass.customAttributes.segment = function (x, y, r, a1, a2) {
    var flag = (a2 - a1) > 180,
        clr = (a2 - a1) / 360;
    a1 = (a1 % 360) * Math.PI / 180;
    a2 = (a2 % 360) * Math.PI / 180;
    return {
        path: [
            ["M", x, y],
            ["l", r * Math.cos(a1), r * Math.sin(a1)],
            ["A", r, r, 0, +flag, 1, x + r * Math.cos(a2), y + r * Math.sin(a2)],
            ["z"]
        ]
    };
};
*/